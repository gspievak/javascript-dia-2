// questão 1
gods.forEach(god => console.log(god.name, god.features.length));

//questão 2

function tipomid(god){
    return god.roles.includes("Mid");
}

let etipomid = gods.filter(tipomid);

console.log(etipomid);

//questão 3
function comparar(a,b){
    if(a.pantheon>b.pantheon){
        return 1;
    }
    if(a.pantheon<b.pantheon){
        return -1;
    }
    if(a.pantheon==b.pantheon){
        return 0;
    }
}
let organiza = gods.sort(comparar);

console.log(organiza);

// questão 4
// os herois estão fora de ordem por causa da questão 3 onde colocou na ordem pelo pantheon
//, optei por não abrir outro arquivo js só para essa parte da questão
let nomeclasse = gods.map((god) =>{return `${god.name} (${god.class})` })
console.log(nomeclasse);